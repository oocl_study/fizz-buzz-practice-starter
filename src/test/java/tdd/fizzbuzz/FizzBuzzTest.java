package tdd.fizzbuzz;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FizzBuzzTest {

    @Test
    public void should_return_normal_number_string_when_countOff_given_normal_number(){

        int number = 1;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("1", res);
    }

    @Test
    public void should_return_Fizz_when_countOff_given_Multiply_Of_3(){

        int number = 3;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("Fizz", res);
    }

    @Test
    public void should_return_Buzz_when_countOff_given_Multiply_Of_5() {

        int number = 5;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("Buzz", res);
    }

    @Test
    public void should_return_Fizz_And_Buzz_when_countOff_given_Multiply_Of_3_And_5() {

        int number = 15;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzBuzz", res);
    }

    @Test
    public void should_return_Whizz_when_countOff_given_Multiply_Of_7() {

        int number = 7;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("Whizz", res);
    }

    @Test
    public void should_return_FizzWhizz_when_countOff_given_Multiply_Of_3_And_7() {

        int number = 21;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzWhizz", res);
    }

    @Test
    public void should_return_BuzzWhizz_when_countOff_given_Multiply_Of_5_And_7() {

        int number = 35;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("BuzzWhizz", res);
    }

    @Test
    public void should_return_FizzBuzzWhizz_when_countOff_given_Multiply_Of_3_And_5_And_7() {

        int number = 210;
        FizzBuzz fizzBuzz = new FizzBuzz();

        String res = fizzBuzz.countOff(number);

        Assertions.assertEquals("FizzBuzzWhizz", res);
    }
}
