package tdd.fizzbuzz;

public class FizzBuzz {

    public static final String FIZZ = "Fizz";
    public static final String BUZZ = "Buzz";
    public static final String WHIZZ = "Whizz";

    public String countOff(int number) {
        String speak = "";

        if(number % 3 == 0){
            speak += FIZZ;
        }
        if(number % 5 == 0){
            speak += BUZZ;
        }
        if(number % 7 ==0){
            speak += WHIZZ;
        }

        if(speak.equals(""))speak += String.valueOf(number);

        return speak;
    }
}
